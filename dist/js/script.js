"use strict";

//1. Створіть новий елемент <a>, задайте йому текст "Learn More" 
//і атрибут href з посиланням на "#". Додайте цей елемент в 
//footer після параграфу.

const a = document.createElement("a");
a.textContent = "Learn More";
a.setAttribute("href", "#");



// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор 
//"rating", і додайте його в тег main перед секцією "Features".
// Створіть новий елемент <option> зі значенням "4" і текстом 
//"4 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "3" і текстом 
//"3 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "2" і текстом 
//"2 Stars", і додайте його до списку вибору рейтингу. 
// Створіть новий елемент <option> зі значенням "1" і текстом 
//"1 Star", і додайте його до списку вибору рейтингу.
// const footer = document.querySelector("footer");

footer.appendChild(a);
const main = document.querySelector("main");

const list = document.createElement("select");
list.id = "rating";


for (let i = 4; i >= 1; i--) {
  let option = document.createElement("option");
  option.textContent = `${i} Stars`;
  option.value = `${i}`;
  list.appendChild(option);
}

    // const featuresSection = document.querySelector("#features");
    // featuresSection.insertAdjacentElement("afterend", main);


main.appendChild(list);
