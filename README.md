## 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

- document.createElement(tagName)
- element.innerHTML
- element.appendChild(newChild)
- element.replaceChild(newChild, oldChild)

## 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

1) Знайти створений елемент, який буде ініціалізований наприклад таким чином: 

`var navigationElement = document.querySelector('.navigation');`

2) Потім щоб його видалити, треба обов'язково звернутись до його батьківсього елементу

`var parentElement = navigationElement.parentNode;`

3) І вже через метод removeChild видаляємо цей клас 

`parentElement.removeChild(navigationElement);`

## 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

- insertAdjacentElement(position position)
- insertAdjacentHTML(position, htmlString)
- insertAdjacentText(position, textString)
- insertBefore(newElement, referenceElement)

